import React from "react";
import { Link } from "react-router-dom";

import "./Button.scss";

interface IButton {
  isLink?: boolean;
  path?: string;
  name: string;
  background: "orange" | "transparent";
  hover?: "square-line" | "darker";
}

export default function Button({
  isLink,
  path,
  name,
  background,
  hover,
}: IButton): JSX.Element {
  return isLink && path ? (
    <Link to={path} className={`Button ${background} ${hover}`}>
      {name}
    </Link>
  ) : (
    <button className={`Button ${background} ${hover}`}>{name}</button>
  );
}
