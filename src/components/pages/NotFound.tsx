import React from "react";

export default function NotFound(): JSX.Element {
  return <>The page is not Found. Try to check url</>;
}
