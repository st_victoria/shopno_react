import React, { useState } from "react";
import clsx from "clsx";

import "./MainPage.scss";
import Routes from "../../enums/routes";
import Button from "../common/Button";

export default function MainPage(): JSX.Element {
  const [inputValue, setInputValue] = useState("");

  const changeValue = (e: any) => setInputValue(e.target.value);

  return (
    <section className="MainPage">
      <section className="MainPage-Title">
        <h1>Новые поступления</h1>
        <p>
          <i>Мы подготовили для Вас лучшие новинки сезона</i>
        </p>
        <Button
          isLink
          path={Routes.CATALOG_NEWS}
          name="Посмотреть новинки"
          background="transparent"
          hover="square-line"
        />
      </section>
      <section className="MainPage-Photos row">
        <div className="MainPage-PhotosCol1">
          <div className="photo1">qqq</div>
          <div className="photo2">www</div>
        </div>
        <div className="MainPage-PhotosCol2">
          <div className="photo1">111</div>
          <div className="photo2">222</div>
          <div className="photo3">333</div>
        </div>
        <div className="MainPage-PhotosCol3">
          <div className="photo1">dffd</div>
          <div className="photo2">fdvd</div>
          <div className="photo3">aws</div>
        </div>
        <div className="MainPage-PhotosCol4">
          <div className="photo1">5432</div>
          <div className="photo2">7654</div>
        </div>
      </section>
      <section className="MainPage-Subscribe">
        <h2>Будь всегда в курсе выгодных предложений</h2>
        <p>
          <i>Подписывайся и следи за новинками и предложениями</i>
        </p>
        <div className="MainPage-InputGroup row">
          <input
            type="email"
            name="email"
            id="email"
            onChange={changeValue}
            value={inputValue}
            className={clsx({ notEmpty: inputValue })}
          />
          <label htmlFor="email">e-mail</label>
          <Button name="Подписаться" background="orange" />
        </div>
      </section>
    </section>
  );
}
