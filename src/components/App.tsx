import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  fab,
  faFacebookF,
  faInstagram,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { faShoppingBag, faUserCircle } from "@fortawesome/free-solid-svg-icons";

import "./App.scss";
import Routes from "../enums/routes";
import Header from "./base/Header";
import MainPage from "./pages/MainPage";
import Catalog from "./pages/Catalog";
import Basket from "./pages/Basket";
import Footer from "./base/Footer";
import NotFound from "./pages/NotFound";

function App(): JSX.Element {
  return (
    <section className="App">
      <Header />
      <main>
        <Switch>
          <Route exact path={Routes.CATALOG} component={Catalog} />
          <Route path={Routes.BASKET} component={Basket} />
          <Route path={Routes.NOT_FOUND} component={NotFound} />
          <Route exact path={Routes.MAIN} component={MainPage} />
          <Redirect to={Routes.NOT_FOUND} />
        </Switch>
      </main>
      <Footer />
    </section>
  );
}

library.add(
  fab,
  faUserCircle,
  faShoppingBag,
  faTwitter,
  faFacebookF,
  faInstagram
);

export default App;
