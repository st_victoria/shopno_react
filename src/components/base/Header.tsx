import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./Header.scss";
import Routes from "../../enums/routes";

export const renderLinks = (linksList: any): JSX.Element =>
  linksList.map((link: any) => (
    <li key={link.id}>
      <Link to={link.path}>{link.name}</Link>
    </li>
  ));

const navLinks = [
  { id: 1, name: "Женщинам", path: `${Routes.CATALOG_WOMEN}` },
  { id: 1, name: "Мужчинам", path: `${Routes.CATALOG_MEN}` },
  { id: 1, name: "Детям", path: `${Routes.CATALOG_KIDS}` },
  { id: 1, name: "Новинки", path: `${Routes.CATALOG_NEWS}` },
  { id: 1, name: "О нас", path: `${Routes.CATALOG_ABOUT}` },
];

export default function Header(): JSX.Element {
  return (
    <header className="Header row">
      <div className="Header-Main row">
        <Link to="/" className="Header-Logo"></Link>
        <nav className="Header-nav">
          <ul className="Header-navList row">{renderLinks(navLinks)}</ul>
        </nav>
      </div>
      <div className="Header-User row">
        <Link to="/account" className="Header-Account row">
          <FontAwesomeIcon icon="user-circle" className="Header-AccountIcon" />
          Войти
        </Link>
        <Link to="/basket" className="Header-Basket row">
          <FontAwesomeIcon icon="shopping-bag" className="Header-BasketIcon" />
          Корзина (0)
        </Link>
      </div>
    </header>
  );
}
