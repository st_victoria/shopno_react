import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./Footer.scss";
import Routes from "../../enums/routes";
import { renderLinks } from "./Header";

const collectionLinks: any = [
  { id: 1, name: "Женщины", path: `${Routes.CATALOG_WOMEN}` },
  { id: 2, name: "Мужчины", path: `${Routes.CATALOG_MEN}` },
  { id: 3, name: "Дети", path: `${Routes.CATALOG_KIDS}` },
  { id: 4, name: "Новости", path: `${Routes.CATALOG_NEWS}` },
];

const shopLinks: any = [
  { id: 1, name: "О нас", path: `${Routes.CATALOG_ABOUT}` },
  { id: 2, name: "Доставка", path: `${Routes.DELIVERY}` },
  { id: 3, name: "Работай с нами", path: `${Routes.COOPERATION}` },
  { id: 4, name: "Контакты", path: `${Routes.CONTACTS}` },
];

export default function Footer(): JSX.Element {
  return (
    <footer className="Footer row">
      <section className="Footer-Collections">
        <h3>Коллекции</h3>
        <ul className="Footer-CollectionsList">
          {renderLinks(collectionLinks)}
        </ul>
      </section>
      <section className="Footer-Shop">
        <h3>Магазин</h3>
        <ul className="Footer-ShopList">{renderLinks(shopLinks)}</ul>
      </section>
      <section className="Footer-Socials">
        <h3>Мы в социальных сетях</h3>
        <p>Сайт разработан в inordic</p>
        <p>2018 Все права защищены</p>
        <div className="Footer-SocialLinks row">
          <a href="https://twitter.com/">
            <FontAwesomeIcon icon={["fab", "twitter"]} />
          </a>
          <a href="https://facebook.com/">
            <FontAwesomeIcon icon={["fab", "facebook-f"]} />
          </a>
          <a href="https://instagram.com/">
            <FontAwesomeIcon icon={["fab", "instagram"]} />
          </a>
        </div>
      </section>
    </footer>
  );
}
