enum Routes {
  MAIN = "/",
  CATALOG = "/catalog",
  CATALOG_MEN = "/catalog/men",
  CATALOG_WOMEN = "/catalog/women",
  CATALOG_KIDS = "/catalog/kids",
  CATALOG_NEWS = "/catalog/news",
  CATALOG_ABOUT = "/catalog/about",
  BASKET = "/basket",
  DELIVERY = "/delivery",
  COOPERATION = "/cooperation",
  CONTACTS = "/contacts",
  NOT_FOUND = "/not-found",
}

export enum RoutesTitle {
  MAIN = "Главная",
  CATALOG = "Каталог",
  BASKET = "Корзина",
}

export default Routes;
